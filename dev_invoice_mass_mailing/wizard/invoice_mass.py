# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Devintelle Software Solutions (<http://devintellecs.com>).
#
##############################################################################

# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
#from openerp.exceptions import except_orm, Warning, RedirectWarning, ValidationError
from odoo.exceptions import UserError

class invoice_mass(models.TransientModel):

    _name="mass.mail.invoice"
    _inherit = ['mail.thread']
    
    @api.model
    def _get_template_id(self):
        temp_id = self.env['mail.template'].search([('name','=','Invoicing: Invoice email')]) 
        if temp_id:
            return temp_id[0]
        return 
    
    
    template_id = fields.Many2one('mail.template', string="Usa template", default=_get_template_id, required=True, select=True)


    @api.multi
    def send_button(self):
        invoice_ids = self.env.context.get('active_ids')
        invoices = self.env['account.invoice'].browse(invoice_ids)
        invoices_not_draft = invoices.filtered(lambda r: r.state != "draft")
        partners = invoices_not_draft.mapped('partner_id')
        for invoice_not_draft in invoices_not_draft:
            if self.template_id.id:
                mtp =self.env['mail.template'].browse(self.template_id.id)
                mtp.send_mail(invoice_not_draft.id,force_send=True)
        return True
            
            
            
            
            

        
        
        
        
        
        
     
        
        
        
        
        
