{
    "name": "Invoice Report Grouped by",
    "summary": "Print invoice lines grouped by picking",
    "version": "11.0.1.1.0",
    "category": "Accounting & Finance",
    "author": "",
    "license": "AGPL-3",
    "depends": [
        "account", "sale",
    ],
    "data": [
        "views/report_invoice.xml",
    ],
    'demo': [
    ],
    'qweb': [
    ],
    'installable': True,
    'application': False,
    'auto_install': False,

}
