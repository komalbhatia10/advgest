# -*- coding: utf-8 -*-

from collections import OrderedDict
from odoo import api, fields, models
from datetime import datetime


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    def get_notation_amt(self, amt):
        amount = str(amt).split('.')
        if len(amount) == 2:
            amount = amount[0] + "," + amount[1]
            return amount
        return amt

    @api.multi
    def get_product_invoice_lines(self, client_ref=False):
        product_invoices = []
        client_order_ref = []
        for line in self.invoice_line_ids:
            stock_move = (False, line)
            if line.sale_line_ids:
                stock_move_ids = self.env['stock.move'].search(
                    [('sale_line_id', '=', line.sale_line_ids[0].id)])
                for stock_move_id in stock_move_ids:
                    stock_move = (stock_move_id, line)
                    client_order_ref.append(stock_move)
        if client_order_ref:
            for ref in client_order_ref:
                if client_ref:
                    for move_line in client_ref.move_lines:
                        if (move_line == ref[0]):
                            product_invoices.append({'price_subtotal': ref[1].price_subtotal_signed,
                                                     'discount': ref[1].discount,
                                                     'default_code': ref[1].product_id.default_code,
                                                     'client_ref': False,
                                                     'description': ref[1].name,
                                                     'qty': self.get_notation_amt(move_line.product_uom_qty),
                                                     'price_unit': self.get_notation_amt("{0:.3f}".format(ref[1].price_unit)),
                                                     })
            if not client_ref:
                # To add the title others.
                # product_invoices.append({'price_subtotal': False,
                #                          'default_code': False,
                #                          'client_ref': 'Others',
                #                          'description': False,
                #                          'qty': False,
                #                          'price_unit': False,
                #                          })
                other_invoice_lines = [
                    line for line in self.invoice_line_ids if not line.sale_line_ids]
                for other_invoice_line in other_invoice_lines:
                    product_invoices.append({'price_subtotal': other_invoice_line.price_subtotal_signed,
                                             'discount': other_invoice_line.discount,
                                             'default_code': other_invoice_line.product_id.default_code,
                                             'client_ref': False,
                                             'description': other_invoice_line.name,
                                             'qty': self.get_notation_amt(other_invoice_line.quantity),
                                             'price_unit': self.get_notation_amt("{0:.3f}".format(other_invoice_line.price_unit)),
                                             })
        else:
            for line in self.invoice_line_ids:
                product_invoices.append({'price_subtotal': line.price_subtotal_signed,
                                         'discount': line.discount,
                                         'default_code': line.product_id.default_code,
                                         'client_ref': False,
                                         'description': line.name,
                                         'qty': self.get_notation_amt(line.quantity),
                                         'price_unit': self.get_notation_amt("{0:.3f}".format(line.price_unit)),
                                         })
        return product_invoices

    @api.multi
    def get_invoice_lines(self):
        vals = []
        stock_pickings = []
        false_stock_pickings = []
        for line in self.invoice_line_ids:
            sale_line = False
            if line.sale_line_ids:
                sale_line = line.sale_line_ids[0].order_id
            if sale_line:
                stock_move_ids = self.env['stock.move'].search(
                    [('sale_line_id', '=', line.sale_line_ids.id)])
                if stock_move_ids:
                    for stock_move_id in stock_move_ids:
                        if stock_move_id.picking_id.state == "done":
                            if self.type=='out_invoice' and stock_move_id.picking_code=='outgoing':
                                stock_pickings.append(stock_move_id.picking_id)
                            elif self.type=='out_refund' and stock_move_id.picking_code=='incoming':
                                stock_pickings.append(stock_move_id.picking_id)
            else:
                false_stock_pickings.append(sale_line)
        stock_pickings = list(set(stock_pickings))
        dict_stock_pickings = {}
        for stock_picking in stock_pickings:
            dict_stock_pickings[stock_picking] = stock_picking.date
        sorteddict_stock_pickings = OrderedDict(
            sorted(dict_stock_pickings.items(), key=lambda x: x[1]))
        stock_pickings = sorteddict_stock_pickings.keys()
        false_stock_pickings = list(set(false_stock_pickings))

        for stock_picking in stock_pickings:
            if stock_picking and self.origin:
                date = datetime.strptime(
                    stock_picking.date, '%Y-%m-%d %H:%M:%S').strftime('%d/%m/%Y')
                client_ref = stock_picking.name + ' - ' + date
                customer_reference = stock_picking.sale_id.client_order_ref
                if customer_reference:
                    client_ref = client_ref + ' - ' + customer_reference
                if stock_picking.picking_type_id.code == 'incoming':
                    client_ref = client_ref + ' Devolució '
                vals.append({'price_subtotal': False, 'default_code': False,
                             'client_ref': client_ref, 'description': False,
                             'qty': False, 'price_unit': False, 'discount':False})
            vals.extend(self.get_product_invoice_lines(
                client_ref=stock_picking))

        # for sort false sale order, display manually invoice line at last
        for so in false_stock_pickings:
            vals.extend(self.get_product_invoice_lines(client_ref=so))
        return [vals, len(vals)]

    @api.multi
    def get_bank_acc_number(self):
        account_number = False
        bank_acc_no = self.env['res.partner.bank'].search(
            [('partner_id', '=', self.partner_id.id)], limit=1)
        if bank_acc_no:
            account_number = bank_acc_no.acc_number[:-4] + "****"
        return account_number
