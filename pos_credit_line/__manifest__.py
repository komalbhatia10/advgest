# -*- coding: utf-8 -*-

{
    'name': 'POS Credit Line',
    'version': '11.0.1.2.28',
    'summary': 'Customisation in POS',
    'sequence': 30,
    'description': """

    """,
    'category': 'Sales',
    'website': '',
    'images': [],
    'depends': ['pos_sale', 'point_of_sale', 'web', 'account', 'sale', 'pos_fast'],
    'data': [
        'data/report_paperformat.xml',
        'report/report_templates.xml',
        'report/pos_sale_report_template.xml',
        'report/report_delivery_pos.xml',
        'views/pos_order_views.xml',
        'views/pos_reports.xml',
        'views/stock_picking_view.xml',
        'views/res_partner_views.xml',
        'views/point_of_sale.xml',
        'views/sale_views.xml',
        'wizard/make_sale_order_views.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [
    ],
    'qweb': [
        'static/src/xml/pos.xml',
        'static/src/xml/digital_sign.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
