# -*- coding: utf-8 -*-

from . import res_partner
from . import pos_order
from . import pos_session
from . import account_invoice
from . import stock_picking
from . import sale_order
