# -*- coding: utf-8 -*-

from odoo import api, fields, models
from datetime import datetime
from dateutil import relativedelta


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.onchange('payment_term_id', 'date_invoice')
    def _onchange_payment_term_date_invoice(self):
        date_invoice = self.date_invoice
        if not date_invoice:
            date_invoice = fields.Date.context_today(self)
        if self.payment_term_id:
            pterm = self.payment_term_id
            pterm_list = pterm.with_context(currency_id=self.company_id.currency_id.id).compute(
                value=1, date_ref=date_invoice)[0]
            date_due = max(line[0] for line in pterm_list)
            if self.partner_id and self.partner_id.day_of_payment:
                actual_due_date = datetime.strptime(date_due, "%Y-%m-%d")
                if self.partner_id.day_of_payment < actual_due_date.day:
                    date_due = actual_due_date + relativedelta.relativedelta(
                        months=1, day=self.partner_id.day_of_payment)
                else:
                    date_due = actual_due_date + relativedelta.relativedelta(
                        day=self.partner_id.day_of_payment)
            self.date_due = date_due
        elif self.date_due and (date_invoice > self.date_due):
            self.date_due = date_invoice

    @api.multi
    def finalize_invoice_move_lines(self, move_lines):
        res = super(AccountInvoice, self).finalize_invoice_move_lines(
            move_lines)
        for mvl in res:
            if mvl[2]['date_maturity']:
                date_due = mvl[2]['date_maturity']
                if self.partner_id and self.partner_id.day_of_payment:
                    actual_due_date = datetime.strptime(date_due, "%Y-%m-%d")
                    if self.partner_id.day_of_payment < actual_due_date.day:
                        date_due = actual_due_date + relativedelta.relativedelta(
                            months=1, day=self.partner_id.day_of_payment)
                    else:
                        date_due = actual_due_date + relativedelta.relativedelta(
                            day=self.partner_id.day_of_payment)
                mvl[2]['date_maturity'] = date_due
        return res
