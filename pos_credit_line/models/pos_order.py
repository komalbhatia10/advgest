# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class PosOrder(models.Model):
    _inherit = 'pos.order'

    state = fields.Selection(selection_add=[('to_be_paid', 'To Be Paid')])
    client_order_ref = fields.Char(string='Customer Reference', copy=False)

    @api.multi
    def action_pos_order_paid(self):
        if not self.test_paid() and not self.partner_id.customer_credit_line:
            raise UserError(_("Order is not paid."))
        self.write({'state': 'paid'})
        if self.partner_id.customer_credit_line:
            self.write({'state': 'to_be_paid'})
        return self.create_picking()

    @api.model
    def get_picking_id(self, order):
        order_ref = self.search([('pos_reference', 'ilike', order['name'])])
        if order_ref and order_ref.picking_id:
            order_ref.picking_id.write(
                {'digital_signature': order['signature'][1]})
            return order_ref.picking_id.id
        else:
            False

    @api.model
    def get_server_id(self, order):
        order_ref = self.search([('pos_reference', 'ilike', order)])
        if order_ref:
            order_ref.action_pos_order_invoice()
            return [order_ref.id]

    @api.model
    def _order_fields(self, ui_order):
        order_fields = super(PosOrder, self)._order_fields(ui_order)
        order_fields['client_order_ref'] = ui_order['client_order_ref']
        return order_fields

    @api.model
    def create_from_ui(self, orders):
        res = super(PosOrder, self).create_from_ui(orders)
        pos_order = self.env['pos.order'].browse(res)
        if pos_order.partner_id.customer_credit_line:
            lines = pos_order.lines
            so = self.env['sale.order'].create({
                'partner_id': pos_order.partner_id.id,
                'origin': pos_order.pos_reference,
                'client_order_ref': pos_order.client_order_ref,
                'date_order': pos_order.date_order,
                'picking_ids': [(6, 0, [pos_order.picking_id.id])],
                'pos_order_ref': pos_order.id,
                'user_id': pos_order.user_id.id,
            })
            for line in lines:
                sale_order_line = self.env['sale.order.line'].create({
                    'product_id': line.product_id.id,
                    'price_unit': line.price_unit,
                    'product_uom_qty': line.qty,
                    'order_id': so.id,
                    'discount': line.discount,
                    'price_subtotal': line.price_subtotal_incl,
                })
                group_id = sale_order_line.order_id.procurement_group_id
                if not group_id:
                    group_id = self.env['procurement.group'].create({
                        'name': sale_order_line.order_id.name, 'move_type': sale_order_line.order_id.picking_policy,
                        'sale_id': sale_order_line.order_id.id,
                        'partner_id': sale_order_line.order_id.partner_shipping_id.id,
                    })
                sale_order_line.order_id.procurement_group_id = group_id
                if pos_order.picking_id.id:
                    if not pos_order.picking_id.group_id:
                        pos_order.picking_id.group_id = group_id
                    stock_move_id = self.env['stock.move'].search(
                        [('product_id', '=', line.product_id.id),
                         ('picking_id', '=', pos_order.picking_id.id)])
                    if stock_move_id:
                        stock_move_id.sale_line_id = sale_order_line
                        stock_move_id.group_id = group_id
                sale_order_line.qty_delivered = sale_order_line._get_delivered_qty()
            sale_order_line.product_id_change()
            so.onchange_partner_id()
            so.user_id = pos_order.user_id or self.env.user
            so._compute_tax_id()
            so.action_confirm()
        return res
