# -*- coding: utf-8 -*-

from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    customer_credit_line = fields.Boolean('Credit Line', copy=False,
                                          help="Helps to know if the person \
                                          has a credit line.")
    digital_signature = fields.Binary(string='Signature')
    day_of_payment = fields.Integer("Day of Payment")
    is_product_photo_report = fields.Boolean('Reports with Product Photos',
                                             copy=False, help="Helps us to know \
                                             weather product photos will be \
                                             printed in reports or not.")

    @api.model
    def save_signature(self, args):
        client_ref = self.search([('id', '=', args['id'])])
        client_ref.digital_signature = args['signature'][1]
