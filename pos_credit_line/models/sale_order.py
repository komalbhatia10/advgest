# -*- coding: utf-8 -*-

from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    client_order_ref = fields.Char(string='Num comanda', copy=False)
    pos_order_ref = fields.Many2one('pos.order', string="Pos Reference")

    @api.multi
    def action_invoice_create(self, grouped=False, final=False):
        res = super(SaleOrder, self).action_invoice_create(grouped, final)
        for so in self:
            if so.pos_order_ref:
                so.pos_order_ref.invoice_id = so.invoice_ids.ids[0]
                if so.pos_order_ref.invoice_id:
                    so.pos_order_ref.sudo().write({'state': 'invoiced'})
        return res
