# -*- coding: utf-8 -*-

from odoo import api, fields, models


class StockPicking(models.Model):
    _inherit = 'stock.picking'
    digital_signature = fields.Binary(string='Signature')

    @api.onchange('digital_signature')
    def onchange_digital_signature(self):
        for picking in self:
            picking.partner_id.write(
                {'digital_signature': picking.digital_signature})
