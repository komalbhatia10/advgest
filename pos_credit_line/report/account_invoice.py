# -*- coding: utf-8 -*-

from collections import OrderedDict
from odoo import api, fields, models
from datetime import datetime


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def get_bank_acc_number(self):
        account_number = False
        bank_acc_no = self.env['res.partner.bank'].search(
            [('partner_id', '=', self.partner_id.id)], limit=1)
        if bank_acc_no:
            account_number = bank_acc_no.acc_number[:-4] + "****"
        return account_number
