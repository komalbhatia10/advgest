odoo.define('pos_credit_line.screens', function (require) {
"use strict";

var PosBaseWidget = require('point_of_sale.BaseWidget');
var screens = require('point_of_sale.screens');
var core = require('web.core');
var _t = core._t;
var gui = require('point_of_sale.gui');
var report
var format
var rpc = require('web.rpc');

screens.ClientListScreenWidget.include({
    line_select: function(event,$line,id){
        var partner = this.pos.db.get_partner_by_id(id);
        if (partner.sale_warn_msg){
            this.gui.show_popup('confirm',{
                        'title': _t("Warning for ") + partner.name,
                        'body': partner.sale_warn_msg,
                    });
        }
        this.$('.client-list .lowlight').removeClass('lowlight');
        if ( $line.hasClass('highlight') ){
            $line.removeClass('highlight');
            $line.addClass('lowlight');
            this.display_client_details('hide',partner);
            this.new_client = null;
            this.toggle_save_button();
        }else{
            this.$('.client-list .highlight').removeClass('highlight');
            $line.addClass('highlight');
            var y = event.pageY - $line.parent().offset().top;
            this.display_client_details('show',partner,y);
            this.new_client = partner;
            this.toggle_save_button();
        }
    },
});
var ReportScreenWidget = screens.ScreenWidget.extend({
    template: 'ReportScreenWidget',

    previous_screen: 'payment',

    init: function(parent, options){
        this._super(parent, options);
        this.$signatureField = this.$(".o_sign_signature").first();
    },
    auto_back: true,
    show: function(){
        var self = this;
        this._super();

        this.renderElement();
        this.details_visible = false;
        this.old_client = this.pos.get_order().get_client();

        this.$('.o_list_report').on('change', function(){
            report = $(this).val();
            self.$('.print').addClass('highlight');
        });
        format = 'paper_format_a5'
        this.$('.o_report_paper_format').on('change', function(){
            format = $(this).val();
        });
    },

    click_print: function(order){
        var self = this;
        var client = this.pos.get_client();
        var order = this.pos.get_order();
        var data = {};
        var signature = self.$(".o_sign_signature").jSignature('getData', 'image');
        if (order && signature){
            if(client){
            data.id = order.get_client().id;
            data.signature = signature;
            rpc.query({
                    model: 'res.partner',
                    method: 'save_signature',
                    args: [data],
                })
        }
        }
        order.finalized = true;
        if (report[0] == 'report_list_invoice'){
            if (client && client.customer_credit_line){
                self.gui.show_popup('alert',{
                        'title': _t('No Invoice'),
                        'body': _t('The Customer selected is Credit Line Customer.'),
                    });
            }
            else{
                var invoiced = this.pos.get_invoice_order(order, format);
            }
        }
        else if (report[0] == 'report_list_purchase_report'){
            var invoiced = this.pos.get_sale_report(order, format)
        }
        else if (report[0] == 'report_list_delivery_slip'){
            var invoiced = this.pos.get_delivery_report(order, format, signature)
        }
        if (invoiced){
            // this.invoicing = true;

            invoiced.fail(function(error){
                self.invoicing = false;
                if (error.message === 'Missing Customer') {
                    self.gui.show_popup('confirm',{
                        'title': _t('Please select the Customer'),
                        'body': _t('You need to select the customer before you can invoice an order.'),
                        confirm: function(){
                            self.gui.show_screen('clientlist');
                        },
                    });
                } else if (error.code < 0) {        // XmlHttpRequest Errors
                    self.gui.show_popup('error',{
                        'title': _t('The order could not be sent'),
                        'body': _t('Check your internet connection and try again.'),
                    });
                } else if (error.code === 200) {    // OpenERP Server Errors
                    self.gui.show_popup('error-traceback',{
                        'title': error.data.message || _t("Server Error"),
                        'body': error.data.debug || _t('The server encountered an error while receiving your order.'),
                    });
                } else {                            // ???
                    self.gui.show_popup('error',{
                        'title': _t("Unknown Error"),
                        'body':  _t("The order could not be sent to the server due to an unknown error"),
                    });
                }
            });

            invoiced.done(function(){
                self.invoicing = false;
            });
        }
    },

    
    renderElement: function(){
        var self = this;
        var linewidget;

        this._super();
        var order = this.pos.get_order();

        this.$signatureField = this.$(".o_sign_signature").first();
        this.$signatureField.jSignature({
            'decor-color': 'transparent',
            'background-color': '#FFF',
            'color': '#000',
            'lineWidth': 2,
            'width': 694,
            'height': 224.87150837988827
        });
        this.emptySignature = self.$signatureField.jSignature("getData");
        this.$('.o_sign_clean').click(function(){
                self.$signatureField.jSignature('reset');
            });

        // this onclick is for the back button
        this.$('.next').click(function(){
            var client = self.pos.get_client();
            if (client && client.customer_credit_line) {
                if( !self.$(".client-customer-reference").val()) {
                self.gui.show_popup('error',_t('A Customer Reference Is Required'));
                return;
                }
            }
            self.pos.push_order(order);
            self.gui.show_screen('receipt');
        });
        this.$('.print').click(function(){
            var client = self.pos.get_client();
            if (client && client.customer_credit_line) {
                if( !self.$(".client-customer-reference").val()) {
                self.gui.show_popup('error',_t('A Customer Reference Is Required'));
                return;
                }
            }
            if (report){
                self.$('.print').addClass('highlight');
                var order = self.pos.get_order();
                if (!order.get_client()){
                    self.gui.show_popup('error',{
                                'title': _t('Please Generate Receipt'),
                                'body': _t('You cannot print report as Customer is not selected.'),
                            });
                }
                else{
                self.click_print();
                }
            }
            if (!report){
                self.gui.show_popup('error',{
                title: _t('Report Not Selected'),
                body:  _t('Please select the report type to be Printed.'),
            });
            }
        });
    },

});

gui.define_screen({name:'report', widget: ReportScreenWidget});

screens.PaymentScreenWidget.include({

    init: function(parent, options) {
        var self = this;
        this._super(parent, options);

        this.pos.bind('change:selectedClient', function() {
            self.start();
        });
    },

    customer_changed: function() {
        var client = this.pos.get_client();
        this.$('.js_customer_name').text( client ? client.name : _t('Customer') );
        if (client && client.customer_credit_line){
            var name = client.name + '(Credit Line)'
            this.$('.js_customer_name').text( client ? name : _t('Customer') );
        }
    },

    start: function(){
        var client = this.pos.get_client();
        $(".js_invoice").hide();
        if (client && client.customer_credit_line){
            $(".paymentmethods-container").hide();
            $(".payment-numpad").hide();
            $(".report-button").show();
        }
        else{
            $(".paymentmethods-container").show();
            $(".payment-numpad").show();
            $(".report-button").hide();
        }
    },
    // called when the order is changed, used to show if
    // the order is paid or not
    order_changes: function(){
        var self = this;
        var order = this.pos.get_order();
        var client = this.pos.get_client();
        if (client && client.customer_credit_line){
            self.$('.next').addClass('highlight');
        }
        else {
            if (!order) {
                return;
            } else if (order.is_paid()) {
                self.$('.next').addClass('highlight');
            }else{
                self.$('.next').removeClass('highlight');
            }
        }
    },

    finalize_validation: function() {
        var self = this;
        var order = this.pos.get_order();

        if (order.is_paid_with_cash() && this.pos.config.iface_cashdrawer) { 

                this.pos.proxy.open_cashbox();
        }

        order.initialize_validation_date();
        order.finalized = true;

        if (order.is_to_invoice()) {
            var invoiced = this.pos.push_and_invoice_order(order);
            this.invoicing = true;

            invoiced.fail(function(error){
                self.invoicing = false;
                if (error.message === 'Missing Customer') {
                    self.gui.show_popup('confirm',{
                        'title': _t('Please select the Customer'),
                        'body': _t('You need to select the customer before you can invoice an order.'),
                        confirm: function(){
                            self.gui.show_screen('clientlist');
                        },
                    });
                } else if (error.code < 0) {        // XmlHttpRequest Errors
                    self.gui.show_popup('error',{
                        'title': _t('The order could not be sent'),
                        'body': _t('Check your internet connection and try again.'),
                    });
                } else if (error.code === 200) {    // OpenERP Server Errors
                    self.gui.show_popup('error-traceback',{
                        'title': error.data.message || _t("Server Error"),
                        'body': error.data.debug || _t('The server encountered an error while receiving your order.'),
                    });
                } else {                            // ???
                    self.gui.show_popup('error',{
                        'title': _t("Unknown Error"),
                        'body':  _t("The order could not be sent to the server due to an unknown error"),
                    });
                }
            });

            invoiced.done(function(){
                self.invoicing = false;
                self.gui.show_screen('report');
            });
        } else {
            // this.pos.push_order(order);
            this.gui.show_screen('report');
        }

    },

    order_is_valid: function(force_validation) {
        var self = this;
        var order = this.pos.get_order();
        var validate  = this._super(force_validation);
        var client = this.pos.get_client()

        // FIXME: this check is there because the backend is unable to
        // process empty orders. This is not the right place to fix it.
        if (order.get_orderlines().length === 0) {
            this.gui.show_popup('error',{
                'title': _t('Empty Order'),
                'body':  _t('There must be at least one product in your order before it can be validated'),
            });
            return false;
        }
        if (client && client.customer_credit_line){
        validate = true;
        }
        return validate
    },

    renderElement: function(){
        var self = this;
        this._super();
        this.$('.report-button').click(function(){
            self.gui.show_screen('report');});
    }
});

});
