# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, models, _
from odoo.exceptions import UserError


class MakeSaleOrder(models.TransientModel):
    _name = "make.sale.order"
    _description = "Make Sales Order"

    @api.multi
    def create_sales_order(self):
        pos_orders = self.env['pos.order'].browse(
            self._context.get('active_ids', []))
        pos_order_state = [
            order.state for order in pos_orders if order.state == 'to_be_paid']
        pos_order_so = [
            order for order in pos_orders if self.env['sale.order'].search([
                ('pos_order_ref', '=', order.id)])]

        if pos_order_so:
            raise UserError(
                _('Please select Pos Orders whose Sale Order are not created.\
                    Sale Order For POS Number %s is \
                     Created.' % pos_order_so[0].name))
        if len(pos_order_state) != len(pos_orders.ids):
            raise UserError(
                _('Please select Pos Orders which are in To be Paid state.'))
        # order = pos_orders and pos_orders[0]
        if pos_orders:
            for pos_order in pos_orders:
                if pos_order.partner_id.customer_credit_line:
                    lines = pos_order.lines
                    so = self.env['sale.order'].create({
                        'partner_id': pos_order.partner_id.id,
                        'origin': pos_order.pos_reference,
                        'client_order_ref': pos_order.client_order_ref,
                        'date_order': pos_order.date_order,
                        'picking_ids': [(6, 0, [pos_order.picking_id.id])],
                        'pos_order_ref': pos_order.id,
                    })
                    for line in lines:
                        sale_order_line = self.env['sale.order.line'].create({
                            'product_id': line.product_id.id,
                            'price_unit': line.price_unit,
                            'product_uom_qty': line.qty,
                            'order_id': so.id,
                            'discount': line.discount,
                            'price_subtotal': line.price_subtotal_incl,
                        })
                        if pos_order.picking_id.id:
                            stock_move_id = self.env['stock.move'].search(
                                [('product_id', '=', line.product_id.id),
                                 ('picking_id', '=', pos_order.picking_id.id)])
                            if stock_move_id:
                                stock_move_id.sale_line_id = sale_order_line
                        sale_order_line.qty_delivered = sale_order_line._get_delivered_qty()
                    sale_order_line.product_id_change()
                    so.onchange_partner_id()
                    so._compute_tax_id()
                    so.action_confirm()
        return {'type': 'ir.actions.act_window_close'}
