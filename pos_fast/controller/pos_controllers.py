# -*- coding: utf-8 -*
from odoo.http import request
from odoo.addons.bus.controllers.main import BusController
from odoo.addons.web.controllers.main import DataSet
from odoo import api, http, SUPERUSER_ID
from odoo.addons.web.controllers.main import ensure_db, Home, Session, WebClient
from odoo.addons.point_of_sale.controllers.main import PosController
import json
import logging
import werkzeug.utils

_logger = logging.getLogger(__name__)

class dataset(DataSet):

    @http.route('/api/pos/install_datas', type='json', auth="user")
    def install_datas(self, model, fields=[], offset=0, limit=False, domain=[], sort=None):
        call_log_object = request.env['pos.call.log']
        min_id = domain[0][2]
        max_id = domain[1][2]
        call_logs = call_log_object.with_context(prefetch_fields=False).search([('call_model', '=', model), ('min_id', '=', min_id), ('max_id', '=', max_id)])
        if call_logs:
            call_log = call_logs[0]
            results = call_log.call_results
            return results
        else:
            record_ids = request.env[model].with_context(prefetch_fields=False).sudo().search(domain, order=sort, limit=limit, offset=offset)
            if 'write_date' not in fields:
                fields.append('write_date')
            results = record_ids.sudo().read(fields)
            results = call_log_object.covert_datetime(model, results)
            vals = {
                'active': True,
                'min_id': min_id,
                'max_id': max_id,
                'call_fields': json.dumps(fields),
                'call_results': json.dumps(results),
                'call_model': model,
                'call_domain': json.dumps(domain),
            }
            call_log_object.create(vals)
            return results

class pos_controller(PosController):

    @http.route('/pos/web', type='http', auth='user')
    def pos_web(self, debug=False, **k):
        session_info = request.env['ir.http'].session_info()
        server_version_info = session_info['server_version_info'][0]
        pos_sessions = None
        if server_version_info == 10:
            pos_sessions = request.env['pos.session'].search([
                ('state', '=', 'opened'),
                ('user_id', '=', request.session.uid),
                ('name', 'not like', '(RESCUE FOR')])
        if server_version_info in [11, 12]:
            pos_sessions = request.env['pos.session'].search([
                ('state', '=', 'opened'),
                ('user_id', '=', request.session.uid),
                ('rescue', '=', False)])
        if not pos_sessions:
            return werkzeug.utils.redirect('/web#action=point_of_sale.action_client_pos_menu')
        pos_session = pos_sessions[0]
        pos_session.login()
        session_info['model_ids'] = {
            'product.pricelist.item': {},
            'product.product': {},
            'res.partner': {},
        }
        first_install = request.env['ir.config_parameter'].sudo().get_param('pos_retail_first_install')
        if not first_install:
            request.env.cr.execute("DELETE FROM pos_cache_database")
            request.env.cr.commit()
            request.env['ir.config_parameter'].sudo().set_param('pos_retail_first_install', 'Done')
        session_info['currency_id'] = request.env.user.company_id.currency_id.id
        model_list = {
            'product.pricelist.item': 'product_pricelist_item',
            'product.product': 'product_product',
            'res.partner': 'res_partner',
        }
        for object, table in model_list.items():
            if table == "product.product":
                products = request.env['product.product'].search([('available_in_pos', '=', True)], order="id desc", limit=1)
                if products:
                    session_info['model_ids'][object]['max_id'] = products[0].id
                products = request.env['product.product'].search([('available_in_pos', '=', True)],
                                                                 order="id", limit=1)
                if products:
                    session_info['model_ids'][object]['min_id'] = products[0].id
            else:
                request.env.cr.execute("select min(id) from %s" % table)
                min_ids = request.env.cr.fetchall()
                session_info['model_ids'][object]['min_id'] = min_ids[0][0] if min_ids and min_ids[0] else 1
                request.env.cr.execute("select max(id) from %s" % table)
                max_ids = request.env.cr.fetchall()
                session_info['model_ids'][object]['max_id'] = max_ids[0][0] if max_ids and max_ids[0] else 1
        context = {
            'session_info': json.dumps(session_info)
        }
        return request.render('point_of_sale.index', qcontext=context)



class pos_bus(BusController):

    def _poll(self, dbname, channels, last, options):
        channels = list(channels)
        channels.append((request.db, 'pos.sync.data', request.uid))
        channels.append((request.db, 'pos.indexed_db', request.uid))
        channels.append((request.db, 'pos.install.database', request.uid))
        return super(pos_bus, self)._poll(dbname, channels, last, options)
