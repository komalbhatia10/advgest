# -*- coding: utf-8 -*-
from . import pos_cache_database
from . import pos_call_log
from . import pos_config
from . import product_product
from . import res_partner
from . import product_pricelist
from . import pos_session
