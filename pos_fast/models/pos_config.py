# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import odoo
import logging
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import json

try:
    to_unicode = unicode
except NameError:
    to_unicode = str

_logger = logging.getLogger(__name__)

class pos_config(models.Model):
    _inherit = "pos.config"

    display_onhand = fields.Boolean('Show qty available product', default=1,
                                    help='Display quantity on hand all products on pos screen')
    large_stocks = fields.Boolean('Large stock', help='If count products bigger than 100,000 rows, please check it')
    allow_order_out_of_stock = fields.Boolean('Allow out-of-stock', default=1,
                                              help='If checked, allow cashier can add product have out of stock')
    allow_of_stock_approve_by_admin = fields.Boolean('Approve allow of stock',
                                                     help='Allow manager approve allow of stock')

    @api.multi
    def remove_database(self):
        for config in self:
            sessions = self.env['pos.session'].search([('config_id', '=', config.id)])
            for session in sessions:
                self.env['bus.bus'].sendmany(
                    [[(self.env.cr.dbname, 'pos.indexed_db', session.user_id.id), json.dumps({
                        'db': self.env.cr.dbname
                    })]])
            self.env.cr.execute("DELETE FROM pos_cache_database")
            self.env.cr.execute("DELETE FROM pos_call_log")
            self.env.cr.commit()
            return {
                'type': 'ir.actions.act_url',
                'url': '/pos/web/',
                'target': 'self',
            }

    def get_fields_by_model(self, model):
        all_fields = self.env[model].fields_get()
        fields_list = []
        for field, value in all_fields.items():
            if field == 'model' or all_fields[field]['type'] in ['one2many', 'binary']:
                continue
            else:
                fields_list.append(field)
        return fields_list

    @api.model
    def install_data(self, model_name=None, min_id=0, max_id=1999, fields_read=[]):
        log_obj = self.env['pos.call.log'].with_context(prefetch_fields=False)
        domain = [('id', '>=', min_id), ('id', '<=', max_id)]
        if model_name == 'product.product':
            domain.append(('available_in_pos', '=', True))
        self.env.cr.execute("select id from pos_call_log where min_id=%s and max_id=%s and call_model='%s'" % (
            min_id, max_id, model_name))
        old_logs = self.env.cr.fetchall()
        if len(old_logs) == 0:
            _logger.info('installing %s from %s to %s' % (model_name, min_id, max_id))
            datas = self.env[model_name].with_context(prefetch_fields=False).search_read(domain, fields_read)
            version_info = odoo.release.version_info[0]
            if version_info == 12:
                all_fields = self.env[model_name].fields_get()
                for data in datas:
                    for field, value in data.items():
                        if field == 'model':
                            continue
                        if all_fields[field] and all_fields[field]['type'] in ['date',
                                                                               'datetime'] and value:
                            data[field] = value.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            vals = {
                'active': True,
                'min_id': min_id,
                'max_id': max_id,
                'call_fields': json.dumps(fields_read),
                'call_results': json.dumps(datas),
                'call_model': model_name,
                'call_domain': json.dumps(domain),
            }
            log_obj.create(vals)
        else:
            old_log_id = old_logs[0][0]
            old_log = log_obj.browse(old_log_id)
            datas = old_log.call_results
        self.env.cr.commit()
        return datas
