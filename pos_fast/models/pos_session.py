# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError
from odoo import SUPERUSER_ID
import json

import logging
_logger = logging.getLogger(__name__)

class pos_session(models.Model):
    _inherit = "pos.session"

    @api.multi
    def action_pos_session_close(self):
        res = super(pos_session, self).action_pos_session_close()
        for session in self:
            self.env['bus.bus'].sendmany(
                [[(self.env.cr.dbname, 'pos.indexed_db', session.user_id.id), json.dumps({
                    'db': self.env.cr.dbname
                })]])
        return res