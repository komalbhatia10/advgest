# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError

import logging

_logger = logging.getLogger(__name__)

class product_template(models.Model):
    _inherit = 'product.template'

    @api.multi
    def write(self, vals):
        res = super(product_template, self).write(vals)
        for product_temp in self:
            products = self.env['product.product'].search([('product_tmpl_id', '=', product_temp.id)])
            for product in products:
                if product.sale_ok and product.available_in_pos:
                    product.sync()
        return res

    @api.multi
    def unlink(self):
        for product_temp in self:
            products = self.env['product.product'].search([('product_tmpl_id', '=', product_temp.id)])
            for product in products:
                data = product.get_data()
                self.env['pos.cache.database'].remove_record(data)
        return super(product_template, self).unlink()


class product_product(models.Model):
    _inherit = 'product.product'

    @api.multi
    def write(self, vals):
        res = super(product_product, self).write(vals)
        for product in self:
            if product and product.id != None and product.sale_ok and product.available_in_pos and product.active:
                product.sync()
            if product.available_in_pos == False or product.active == False:
                data = product.get_data()
                self.env['pos.cache.database'].remove_record(data)
        return res

    def get_data(self):
        cache_obj = self.env['pos.cache.database']
        fields_sale_load = cache_obj.get_fields_by_model(self._inherit)
        data = self.read(fields_sale_load)[0]
        data['model'] = self._inherit
        return data

    @api.model
    def sync(self):
        data = self.get_data()
        self.env['pos.cache.database'].sync_to_pos(data)
        return True

    @api.model
    def create(self, vals):
        product = super(product_product, self).create(vals)
        if product.sale_ok and product.available_in_pos:
            product.sync()
        return product

    @api.multi
    def unlink(self):
        for product in self:
            data = product.get_data()
            self.env['pos.cache.database'].remove_record(data)
        return super(product_product, self).unlink()
