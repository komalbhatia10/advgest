import xmlrpclib
import time
import logging

__logger = logging.getLogger(__name__)

start_time = time.time()

database = '11_pos_fast_100k_products'
login = 'admin@mail.com'
password = '1234'
url = 'http://localhost:8888'

common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
uid = common.authenticate(database, login, password, {})

models = xmlrpclib.ServerProxy(url + '/xmlrpc/object')
with open("img.png", "rb") as f:
    data = f.read()
    for i in range(0, 100):
        vals = {
            'street': u'255 Bui Huu Nghia, Tan Van',
            'city': u'Bien Hoa',
            'name': 'Test - %s' % str(i),
            'zip': u'False',
            'mobile': u'0909888888',
            'country_id': 233,
            'image': data.encode("base64")
        }
        models.execute_kw(database, uid, password, 'res.partner', 'create', [vals])
        print i


