"use strict";
odoo.define('pos_fast.screen_client_list', function (require) {
    var screens = require('point_of_sale.screens');

    screens.ClientListScreenWidget.include({
        start: function () {
            var self = this;
            this._super();
            this.pos.bind('sync:partner', function (partner_data) {
                console.log('screen_client_list: sync partner');
                self.pos.database['res.partner'] = _.filter(self.pos.database['res.partner'], function (partner) {
                    return partner.id != partner_data['id']
                });
                self.pos.database['res.partner'].push(partner_data);
                self.partner_cache.cache_node(partner_data['id'], null);
                self.pos.db.add_partners([partner_data])
            });
        }
    });

});