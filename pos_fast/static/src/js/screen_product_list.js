"use strict";
odoo.define('pos_fast.screen_product_list', function (require) {

    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');
    var utils = require('web.utils');
    var round_pr = utils.round_precision;

    screens.ProductListWidget.include({
        init: function (parent, options) {
            var self = this;
            this._super(parent, options);
            this.pos.bind('sync:product', function (product_data) {
                console.log('screen_product_list: sync product');
                if (self.pos.server_version == 10) {
                    self.pos.db.add_products([product_data]);
                    self.pos.db.product_by_id[product_data['id']] = product_data;
                    self.product_cache.cache_node(product_data['id'], null);
                    var product_node = self.render_product(product_data);
                    product_node.addEventListener('click', self.click_product_handler);
                    var $product_el = $(".product-list " + "[data-product-id='" + product_data['id'] + "']");
                    if ($product_el.length > 0) {
                        $product_el.replaceWith(product_node);
                    }
                }
                if ([11, 12].indexOf(self.pos.server_version) != -1) {
                    var using_company_currency = self.pos.config.currency_id[0] === self.pos.company.currency_id[0];
                    if (self.pos.company_currency) {
                        var conversion_rate = self.pos.currency.rate / self.pos.company_currency.rate;
                    } else {
                        var conversion_rate = 1;
                    }
                    self.pos.db.add_products(_.map([product_data], function (product) {
                        if (!using_company_currency) {
                            product.lst_price = round_pr(product.lst_price * conversion_rate, self.pos.currency.rounding);
                        }
                        product.categ = _.findWhere(self.pos.product_categories, {'id': product['categ_id'][0]});
                        var product = new models.Product({}, product);
                        var current_pricelist = self._get_active_pricelist();
                        var cache_key = self.calculate_cache_key(product, current_pricelist);
                        self.product_cache.cache_node(cache_key, null);
                        var product_node = self.render_product(product);
                        product_node.addEventListener('click', self.click_product_handler);
                        var contents = document.querySelector(".product-list " + "[data-product-id='" + product['id'] + "']");
                        if (contents) {
                            contents.replaceWith(product_node)
                        }
                        return product;
                    }));
                }
            });
        }
    });
});
