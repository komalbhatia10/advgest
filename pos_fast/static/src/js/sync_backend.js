odoo.define('pos_fast.sync_backend', function (require) {
    var exports = {};
    var Backbone = window.Backbone;
    var bus = require('bus.bus');
    var indexed_db = require('pos_fast.indexedDB');
    var db = require('point_of_sale.DB');
    var models = require('point_of_sale.models');

    exports.sync_backend = Backbone.Model.extend({ // chanel 2: pos sync backend
        initialize: function (pos) {
            this.pos = pos;
        },
        start: function () {
            this.bus = bus.bus;
            this.bus.last = this.pos.db.load('bus_last', 0);
            this.bus.on("notification", this, this.on_notification);
            this.bus.start_polling();
        },
        on_notification: function (notifications) {
            if (notifications && notifications[0] && notifications[0][1]) {
                for (var i = 0; i < notifications.length; i++) {
                    var channel = notifications[i][0][1];
                    if (channel == 'pos.sync.data') {
                        this.sync_with_backend(notifications[i][1]);
                    }
                }
            }
        },
        sync_with_backend: function (data) {
            var model = data['model'];
            var old_caches = this.pos.database[model];
            var new_caches = _.filter(old_caches, function (cache) {
                return cache['id'] != data['id'];
            });
            if (!data['deleted']) {
                new_caches.push(data);
            }
            this.pos.database[model] = new_caches;
            if (model && (data.deleted == false || !data.deleted)) {
                indexed_db.write(model, [data]);
                if (model == 'product.product') {
                    this.pos.trigger('sync:product', data);
                }
                if (model == 'res.partner') {
                    this.pos.trigger('sync:partner', data);
                }
                if (model == 'product.pricelist.item') {
                    this.pos.syncing_pricelist_item(data)
                }
            }
            if (model && data.deleted == true) {
                indexed_db.unlink(model, data);
            }
        }
    });

    var _super_posmodel = models.PosModel.prototype;
    models.PosModel = models.PosModel.extend({
        load_server_data: function () {
            var self = this;
            return _super_posmodel.load_server_data.apply(this, arguments).then(function () {
                self.sync_backend = new exports.sync_backend(self);
                self.sync_backend.start();
            })
        },
        syncing_pricelist_item: function (pricelist_item) {
            var pricelist_by_id = {};
            _.each(this.pricelists, function (pricelist) {
                pricelist_by_id[pricelist.id] = pricelist;
            });
            var pricelist = pricelist_by_id[pricelist_item.pricelist_id[0]];
            if (pricelist) {
                if (pricelist_item.base_pricelist_id) {
                    pricelist_item['base_pricelist'] = pricelist_by_id[pricelist_item.base_pricelist_id[0]];;
                }
                var append_items = false;
                for (var i = 0; i < pricelist.items.length; i++) {
                    if (pricelist.items[i]['id'] == pricelist_item['id']) {
                        pricelist.items[i] = pricelist_item;
                        append_items = true
                    }
                }
                if (append_items == false) {
                    pricelist.items.push(pricelist_item);
                }
            }
        },
    });

    return exports;
});
