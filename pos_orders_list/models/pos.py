# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import Warning
import random
from datetime import date, datetime


class pos_order(models.Model):
    _inherit = 'pos.order'
    
    def return_new_order(self):
       lines = []
       for ln in self.lines:
           lines.append(ln.id)
       
       vals = {
            'amount_total': self.amount_total,
            'date_order': self.date_order,
            'id': self.id,
            'name': self.name,
            'partner_id': [self.partner_id.id, self.partner_id.name],
            'pos_reference': self.pos_reference,
            'state': self.state,
            'session_id': [self.session_id.id, self.session_id.name],
            'company_id': [self.company_id.id, self.company_id.name],
            'lines': lines,
       }
       return vals
    
    def return_new_order_line(self):
       
       orderlines = self.env['pos.order.line'].search([('order_id.id','=', self.id)])
       
       final_lines = []
       #for ln in self.lines:
           #lines.append(ln.id)
       #print "lllllllllllllllliiiiiiiiiiinnnnnnnnnnnnnneeeeeeeessssssssssssss",orderlines
       
       for l in orderlines:
           vals1 = {
                'discount': l.discount,
                'id': l.id,
                'order_id': [l.order_id.id, l.order_id.name],
                'price_unit': l.price_unit,
                'product_id': [l.product_id.id, l.product_id.name],
                'qty': l.qty,
           }
           final_lines.append(vals1)
           
       return final_lines   


class pos_config(models.Model):
    _inherit = 'pos.config'
    
    pos_session_limit = fields.Selection([('all',  "Load all Session's Orders"), ('last3', "Load last 3 Session's Orders"), ('last5', " Load last 5 Session's Orders")], string='Session limit')
    
