 

Version 11.0.0.6:
		- Update date format in orderlist and popup.

Version 11.0.1.6:
		- Add new feature to restrict on pos orders to load.

Version 11.0.1.7:
		- Update search view and correct session load count.
